from SimpleCV import Image,  Display

display = Display()

img = Image("one.jpg").binarize().invert().erode(6)

blobs = img.findBlobs()

blobs = blobs.sortArea()

bigBlobArea = 0
for blob in blobs:
  print blob.area()
  if bigBlobArea < blob.area(): 
    bigBlobArea = blob.area()
    card = blob

print "count = ", blobs.count()

filterBlobs = blobs.filter(blobs.area() >= card.area())

print "Blobs contain:  "
print filterBlobs

while not display.isDone():
  filterBlobs.show(width=3)
   

